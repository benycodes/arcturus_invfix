package com.eu.habbo.messages.incoming.inventory;

import com.eu.habbo.messages.incoming.MessageHandler;
import com.eu.habbo.messages.outgoing.inventory.InventoryItemsComposer;

public class RequestInventoryItemsEvent extends MessageHandler
{
    @Override
    public void handle() throws Exception
    {
        int amountOfPacketsToSend = (int)Math.floor(this.client.getHabbo().getHabboInventory().getItemsComponent().getItems().size() / (float)700);
        for(int i = 0; i < amountOfPacketsToSend; i++) {
            this.client.sendResponse(new InventoryItemsComposer(this.client.getHabbo(), amountOfPacketsToSend, i));
        }
    }
}
